function x = nz_assignment(V, H, X, phi, psi)
% Returns an assignment 'x' with non-zero probability
x = randperm(V);
p_x = probability(x, V, H, phi, psi);
while p_x == 0
    for i = 1:V
        x(i) = round(rand*(X-1) + 1);
        p_x = probability(x, V, H, phi, psi);
        if p_x > 0
            break;
        end
    end
end
end

function p = probability(x, V, H, phi, psi)
% Returns the weight of the assignment 'x'
[u, v] = ind2sub(size(H), find(triu(H)==1));
E = [u, v];
p = 1;
for i = 1:V
    p = p * phi(i, x(i));
end
for e = E'
    i = e(1);
    j = e(2);
    p = p * psi(x(i), x(j));
end
end