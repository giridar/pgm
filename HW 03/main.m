function m_ag = main()
% Returns the probability that x_a=g for different values of 'burnin' & 'its'
H = [0 1 1 1; 1 0 0 1; 1 0 0 1; 1 1 1 0];
G = [0 1 1 1; 1 0 1 1; 1 1 0 1; 1 1 1 0];
w = zeros(4, 4);

set = [2^6, 2^10, 2^14, 2^18];
m = gibbs(H, G, w, set, set);
m_ag = zeros(4, 4);
for burnin = 1:4
    for its = 1:4
        m_ag(burnin, its) = m{burnin, its}(1, 3);
    end
end
end