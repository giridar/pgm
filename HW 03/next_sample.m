function x = next_sample(x, order, H, X, phi, psi)
% Returns the next sample of 'x'
% Order of variables is from 1 to V
for i = order
    % Draw a sample for variable i conditioned on the most recent value of all
    % other variables. Since this is a MRF, a node is independent of all other
    % nodes given its neighbors.
    dist = cond_dist(i, neighbors(i, H), X, x, phi, psi);
    x(i) = randsample(1:X, 1, true, dist);
end
end

function N = neighbors(i, H)
% Returns the neighbors of node 'i' in 'H'
[~, N] = find(H(i, :));
end

function dist = cond_dist(i, N, X, x, phi, psi)
% Returns the weight vector (unnormalised probabilities) of conditional distribution 'i' given 'N'
dist = zeros(1, X);
for x_i = 1:X
    p = phi(i, x_i);
    for j = N
        p = p * psi(x_i, x(j));
    end
    dist(x_i) = p;
end
end