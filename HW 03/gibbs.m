function m = gibbs(H, G, w, burnin, its)
% Returns a 'b' x 'i' cell array of approximate marginal distributions
V = size(H, 1);
X = size(G, 1);

phi = exp(w);
psi = G;

% Random non-zero initial assignment to 'x'
x = nz_assignment(V, H, X, phi, psi)

% Assign an order of the variables
order = 1:V;

% Generate all 'burnin' + 'its' samples
max_its = max(burnin) + max(its);
samples = zeros(max_its, V);
for t = 1:max_its
    x = next_sample(x, order, H, X, phi, psi);
    samples(t, :) = x;
end

% Approximate the marginals using only the 'its' samples
B = size(burnin, 2);
T = size(its, 2);
m = cell(B, T);
for b = 1:B
    for t = 1:T
        from = burnin(b) + 1;
        to = burnin(b) + its(t);
        m{b, t} = marginal_dist(V, X, samples(from:to, :));
    end
end
end

function dist = marginal_dist(V, X, samples)
% Returns the marginal distribution of the variables observed in the samples
dist = zeros(V, X);
N = size(samples, 1);
for i = 1:V
    for x_i = 1:X
        dist(i, x_i) = size(find(samples(:, i)==x_i), 1);
    end
end
dist = dist / N;
end