function w = homem(H, G, i, samples)
V = size(H, 1);
X = size(G, 1);
% Empirical count
[M, N] = count(V, X, i, samples);
% Sample validity matrix for every assignment to x_i
svm = validity(H, G, i, samples);

% Iterations for BP
its = 100;

% Outer loop for Expectation Maximization
% Random initial weights
w0_em = zeros(V, X);
[b_i0_em, Z0_em] = sumprod(H, G, w0_em, its);
ll0_em = LL(w0_em, Z0_em, i, M, N, samples, svm);

while 1;
    % Inner loop for Gradient Ascent
    % Use Initial weights of EM
    w0_ga = w0_em;
    b_i0_ga = b_i0_em;
    ll0_ga = ll0_em;
    
    t_ga = 0;
    while 1
        w1_ga = zeros(V, X);
        gamma = 2 / (2+t_ga);
        for j = 1:V
            sum_j = 0;
            for x_j = 1:X
                w1_ga(j, x_j) = w0_ga(j, x_j) + gamma * dLL(j, x_j, w0_ga, b_i0_ga, i, M, N, samples, svm);
                sum_j = sum_j + w1_ga(j, x_j);
            end
        end
        w1_ga = norm(w1_ga);
        
        [b_i1_ga, Z1_ga] = sumprod(H, G, w1_ga, its);
        ll1_ga = LL(w1_ga, Z1_ga, i, M, N, samples, svm);
        if ll1_ga - ll0_ga < 0.001
            % No significant increase in the log-likelihood
            w1_em = w0_ga;
            break;
        end
        w0_ga = w1_ga;
        b_i0_ga = b_i1_ga;
        ll0_ga = ll1_ga;
    end
    
    [b_i1_em, Z1_em] = sumprod(H, G, w1_em, its);
    ll1_em = LL(w1_em, Z1_em, i, M, N, samples, svm);
    if ll1_em - ll0_em < 0.001
        % No significant increase in the log-likelihood
        w = w0_em;
        break;
    end
    w0_em = w1_em;
    b_i0_em = b_i1_em;
    ll0_em = ll1_em;
end
end


function w = norm(w)
% Normalize the weights
for k = 1:size(w, 1)
    w(k, :) = w(k, :) - min((w(k, :)));
    max_w = max((w(k, :)));
    if max_w > 0
        w(k, :) = w(k, :) / max_w;
    end
end
end


function [M, N] = count(V, X, i, samples)
% Returns the size and count of the variables observed in the samples
M = size(samples, 1);
N = zeros(V, X);
for j = [1:i-1, i+1:V]
    for x_j = 1:X
        N(j, x_j) = size(find(samples(:, j)==x_j), 1);
    end
end
end


function svm = validity(H, G, i, samples)
% Returns the validity of every sample for every assignment of x_i
X = size(G, 1);
K = size(samples, 1);
svm = zeros(K, X);
N_i = neighbors(i, H);
for k = 1:K
    sample = samples(k, :);
    for x_i = 1:X
        is_valid = 1;
        for j = N_i
            is_valid = is_valid * G(x_i, sample(j));
        end
        svm(k, x_i) = is_valid;
    end
end
end


function N = neighbors(i, H)
% Returns the neighbors of node 'i' in 'H'
[~, N] = find(H(i, :));
end


function dll = dLL(j, x_j, w, b_i, i, M, N, samples, svm)
% Partial derivative of Log Likelihood wrt w_{k,x_k}
dll = 0;
b_j_xj = b_i(j, x_j);
[K, ~] = size(samples);
if j == i
    for k = 1:K
        p_xi_k = exp(w(i, :)) .* svm(k, :);
        p_xi_k = p_xi_k / sum(p_xi_k);
        is_j_xj = svm(k, x_j);
        % Expected count of x_i
        dll = dll + p_xi_k(x_j) * is_j_xj;
    end
else
    dll = N(j, x_j);
end
dll = dll - M * b_i(j, x_j);
end


function ll = LL(w, Z, i, M, N, samples, svm)
% Log likelihood
ll = sum(sum(N .* w)) - M * log(Z);
[K, V] = size(samples);
sw = size(w);
for k = 1:K
    sample = samples(k, :);
    p_xi_k = exp(w(i, :)) .* svm(k, :);
    p_xi_k = p_xi_k / sum(p_xi_k);
    % Expected value of w_{i,x_i}
    exp_wi = sum(p_xi_k .* w(i, :));
    ll = ll + exp_wi;
end
end