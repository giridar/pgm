function w = hommle(H, G, samples)
V = size(H, 1);
X = size(G, 1);
% Empirical count
[M, N] = count(V, X, samples);

% Iterations for BP
its = 100;

% Random initial weights
w0 = zeros(V, X);
[b_i0, Z0] = sumprod(H, G, w0, its);
ll0 = LL(M, N, w0, Z0);

t = 0;
while 1
    w1 = zeros(V, X);
    gamma = 2 / (2+t);
    for k = 1:V
        sum_k = 0;
        for x_k = 1:X
            w1(k, x_k) = w0(k, x_k) + gamma * dLL(k, x_k, N, M, b_i0);
            sum_k = sum_k + w1(k, x_k);
        end
    end
    w1 = norm(w1);
    
    [b_i1, Z1] = sumprod(H, G, w1, its);
    ll1 = LL(M, N, w1, Z1);
    if ll1 - ll0 < 0.001
        % No significant increase in the log-likelihood
        w = w0;
        break;
    end
    w0 = w1;
    b_i0 = b_i1;
    ll0 = ll1;
end
end


function w = norm(w)
% Normalize the weights
for k = 1:size(w, 1)
    w(k, :) = w(k, :) - min((w(k, :)));
    max_w = max((w(k, :)));
    if max_w > 0
        w(k, :) = w(k, :) / max_w;
    end
end
end


function [M, N] = count(V, X, samples)
% Returns the size and count of the variables observed in the samples
M = size(samples, 1);
N = zeros(V, X);
for j = 1:V
    for x_j = 1:X
        N(j, x_j) = size(find(samples(:, j)==x_j), 1);
    end
end
end


function dll = dLL(k, x_k, N, M, b_i)
% Partial derivative of Log Likelihood wrt w_{k,x_k}
dll = N(k, x_k) - M * b_i(k, x_k);
end


function ll = LL(M, N, w, Z)
% Log likelihood
ll = sum(sum(N .* w)) - M * log(Z);
end