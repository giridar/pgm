function samples = gibbs(H, G, w, burnin, its)
% Returns a 'b' x 'i' cell array of approximate marginal distributions
V = size(H, 1);
X = size(G, 1);

phi = exp(w);
psi = G;

% Random non-zero initial assignment to 'x'
x = nz_assignment(V, H, X, phi, psi);

% Assign an order of the variables
order = 1:V;

% Generate 'burnin' samples
for t = 1:burnin
    x = next_sample(x, order, H, X, phi, psi);
end
% Generate 'its' samples
samples = zeros(its, V);
for t = 1:its
    x = next_sample(x, order, H, X, phi, psi);
    samples(t, :) = x;
end
end


function x = next_sample(x, order, H, X, phi, psi)
% Returns the next sample of 'x'
% Order of variables is from 1 to V
for i = order
    % Draw a sample for variable i conditioned on the most recent value of all
    % other variables. Since this is a MRF, a node is independent of all other
    % nodes given its neighbors.
    dist = cond_dist(i, neighbors(i, H), X, x, phi, psi);
    x(i) = randsample(1:X, 1, true, dist);
end
end


function dist = cond_dist(i, N, X, x, phi, psi)
% Returns the weight vector (unnormalised probabilities) of conditional distribution 'i' given 'N'
dist = zeros(1, X);
for x_i = 1:X
    p = phi(i, x_i);
    for j = N
        p = p * psi(x_i, x(j));
    end
    dist(x_i) = p;
end
end


function N = neighbors(i, H)
% Returns the neighbors of node 'i' in 'H'
[~, N] = find(H(i, :));
end


function x = nz_assignment(V, H, X, phi, psi)
% Returns an assignment 'x' with non-zero probability
x = zeros(1, V);
for i = 1:V
    x(i) = randi(X);
end
p_x = probability(x, V, H, phi, psi);
while p_x == 0
    for i = 1:V
        x(i) = randi(X);
        p_x = probability(x, V, H, phi, psi);
        if p_x > 0
            break;
        end
    end
end
end


function p = probability(x, V, H, phi, psi)
% Returns the weight of the assignment 'x'
[u, v] = ind2sub(size(H), find(triu(H)==1));
E = [u, v];
p = 1;
for i = 1:V
    p = p * phi(i, x(i));
end
for e = E'
    i = e(1);
    j = e(2);
    p = p * psi(x(i), x(j));
end
end