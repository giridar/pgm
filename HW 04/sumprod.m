function [b_i, Z] = sumprod(H, G, w, its)
V = size(H, 1);
% Edges in the graph H
[v, u] = ind2sub(size(H), find(tril(H)==1));
E = [u, v];
X = size(G, 1);

% Potential functions
phi = exp(w);
psi = G;

% Beliefs & Approximate Partition function
[b_i, b_ij] = beliefs(V, E, H, X, phi, psi, its);
Z = exp(-bfe(V, E, X, w, psi, b_i, b_ij));
end


function [b_i, b_ij] = beliefs(V, E, H, X, phi, psi, its)
% Loopy message update
m = rand([size(H), X]);
for t = 1:its
    prev_m = m;
    m = zeros([size(H), X]);
    for ij = E'
        i = ij(1);
        j = ij(2);
        [~, N_i] = find(H(i, :));
        [~, N_j] = find(H(j, :));
        sum_ij = 0;
        sum_ji = 0;
        for x_j = 1:X
            m(i, j, x_j) = msg(X, N_i, phi, psi, prev_m, i, j, x_j);
            m(j, i, x_j) = msg(X, N_j, phi, psi, prev_m, j, i, x_j);
            sum_ij = sum_ij + m(i, j, x_j);
            sum_ji = sum_ji + m(j, i, x_j);
        end
        m(i, j, :) = m(i, j, :) / sum_ij;
        m(j, i, :) = m(j, i, :) / sum_ji;
    end
end

% Belief calculation of the nodes
b_i = zeros(V, X);
for i = 1:V
    [~, N_i] = find(H(i, :));
    sum = 0;
    for x_i = 1:X
        product = 1;
        for k = N_i
            product = product * m(k, i, x_i);
        end
        b_i(i, x_i) = phi(i, x_i) * product;
        sum = sum + b_i(i, x_i);
    end
    b_i(i, :) = (1 / sum) * b_i(i, :);
end

% Belief calculation of the edges
b_ij = zeros(V, V, X, X);
for ij = E'
    i = ij(1);
    j = ij(2);
    [~, N_i] = find(H(i, :));
    [~, N_j] = find(H(j, :));
    sum = 0;
    for x_i = 1:X
        for x_j = 1:X
            product = 1;
            for k = N_i
                if k ~= j
                    product = product * m(k, i, x_i);
                end
            end
            for k = N_j
                if k ~= i
                    product = product * m(k, j, x_j);
                end
            end
            b_ij(i, j, x_i, x_j) = phi(i, x_i) * phi(j, x_j) * psi(x_i, x_j) * product;
            sum = sum + b_ij(i, j, x_i, x_j);
        end
    end
    b_ij(i, j, :, :) = (1 / sum) * b_ij(i, j, :, :);
end
end


function m = msg(X, N_i, phi, psi, prev_m, i, j, x_j)
% Meesage passed by a single node for all values X
m = 0;
for x_i = 1:X
    product = 1;
    for k = N_i
        if (k ~= j)
            product = product * prev_m(k, i, x_i);
        end
    end
    m = m + phi(i, x_i) * psi(x_i, x_j) * product;
end
end


function Fb = bfe(V, E, X, log_phi, psi, T_i, T_ij)
% Bethe Free Energy
Fb = 0;
for i = 1:V
    for x_i = 1:X
        t_i = T_i(i, x_i);
        if t_i > 0
            Fb = Fb - t_i * (log_phi(i, x_i) - log(t_i));
        end
    end
end

for ij = E'
    i = ij(1);
    j = ij(2);
    for x_i = 1:X
        for x_j = 1:X
            t_i = T_i(i, x_i);
            t_j = T_i(j, x_j);
            t_ij = T_ij(i, j, x_i, x_j);
            if t_ij > 0
                Fb = Fb - t_ij * (log(psi(x_i, x_j)) - log(t_ij/(t_i*t_j)));
            end
        end
    end
end
end